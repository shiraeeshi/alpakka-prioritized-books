package com.example

import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

import com.typesafe.config.ConfigFactory

import akka.actor.{ ActorSystem, Props, DeadLetter }
import akka.stream.{ActorMaterializer, ClosedShape}
import akka.stream.alpakka.amqp.{AmqpConnectionUri, IncomingMessage, OutgoingMessage, NamedQueueSourceSettings, QueueDeclaration, AmqpSinkSettings}
import akka.stream.alpakka.amqp.scaladsl.AmqpSink
import akka.stream.alpakka.amqp.scaladsl.AmqpSource
import akka.stream.scaladsl._
import akka.util.ByteString

import com.rabbitmq.client.AMQP.BasicProperties

object BooksPublisherDomain {
  case class Book(title: String, author: String, priority: Int)

  val books = List(
    Book("good book 1", "good author 1", 5),
    Book("good book 2", "good author 2", 5),
    Book("good book 3", "good author 3", 5),
    Book("good book 4", "good author 4", 5),
    Book("bad book 1", "bad author 1", 1),
    Book("bad book 2", "bad author 2", 1),
    Book("bad book 3", "bad author 3", 1),
    Book("bad book 4", "bad author 4", 1),
    Book("best book 1", "best author 1", 9),
    Book("best book 2", "best author 2", 9),
    Book("best book 3", "best author 3", 9),
    Book("best book 4", "best author 4", 9))
}
object PrioritizedBooksPublisher extends App {
  import BooksPublisherDomain._

  implicit val actorSystem = ActorSystem()
  implicit val actorMaterializer = ActorMaterializer()

  private val configFactory = ConfigFactory.load()

  val host = configFactory.getString("custom.rabbitmq.host")
  val port = configFactory.getInt("custom.rabbitmq.port")

  val queueName = "prioritizedBooksQueue"
  //val queueName = "newBooksQueue1"
  val queueDeclaration = QueueDeclaration(queueName, durable = true).withArguments("x-max-priority" -> Int.box(10))
  val uri = s"amqp://guest:guest@$host:$port/my-rabbit"
  val amqpUri = AmqpConnectionUri(uri)

  val amqpSink = AmqpSink(AmqpSinkSettings(amqpUri).withRoutingKey(queueName).withDeclarations(queueDeclaration))

  val source = Source(books.map( book =>
      OutgoingMessage(
        ByteString(book.toString),
        false,
        false,
        Some(new BasicProperties()
              .builder()
              .priority(book.priority)
              .build())))) .map { book => println(book); book}

  /*
  val graph = RunnableGraph.fromGraph(GraphDSL.create(amqpSink) { implicit builder => s =>
    import GraphDSL.Implicits._
    source ~> s.in
    ClosedShape
  })
  val future = graph.run()
  */
  val future = source.runWith(amqpSink)
  future.onComplete { _ =>
    actorSystem.terminate()
  }

  Await.result(actorSystem.whenTerminated, Duration.Inf)
}

object BooksConsumer extends App {
  import BooksPublisherDomain._

  implicit val actorSystem = ActorSystem()
  implicit val actorMaterializer = ActorMaterializer()

  private val configFactory = ConfigFactory.load()

  val host = configFactory.getString("custom.rabbitmq.host")
  val port = configFactory.getInt("custom.rabbitmq.port")

  val queueName = "prioritizedBooksQueue"
  val queueDeclaration = QueueDeclaration(queueName, durable = true).withArguments("x-max-priority" -> Int.box(10))
  val uri = s"amqp://guest:guest@$host:$port/my-rabbit"
  val amqpUri = AmqpConnectionUri(uri)

  val namedQueueSourceSettings = NamedQueueSourceSettings(amqpUri, queueName).withDeclarations(queueDeclaration)
  val source = AmqpSource.atMostOnceSource(namedQueueSourceSettings, bufferSize = 1)

  val flow1 = Flow[IncomingMessage].map(_.bytes)
  val flow2 = Flow[ByteString].map(_.utf8String)
  val sink = Sink.foreach[String](println)

  source.via(flow1).via(flow2).to(sink).run()

}
